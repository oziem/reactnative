import { NavigationActions } from 'react-navigation'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as todoActions from '../actionsToDo'
import AddTaskForm from '../../components/AddTaskForm'

class TaskForm extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            task: '',
        }
        this.onCancel = this.onCancel.bind(this)
        this.onAdd = this.onAdd.bind(this)
        this.onChangeText = this.onChangeText.bind(this)
    }

    onCancel() {
        this.props.navigation.dispatch(NavigationActions.back());
    }

    onAdd() {
        this.props.actions.addTodo({ task: this.state.task });
        this.onCancel();
    }

    onChangeText(text) {
        this.setState({ task: text })
    }

    render() {
        return (
            <AddTaskForm 
                onAdd={this.onAdd}
                onCancel={this.onCancel}
                onChangeText={this.onChangeText}/>
        );
    }
}

TaskForm.propTypes = {
    actions: PropTypes.object.isRequired,
}

function mapStateToProps(state, ownProps) {
  return { };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(todoActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);