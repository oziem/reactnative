import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import TaskList from '../../components/TaskList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as todoActions from '../actionsToDo'

class PluralTodo extends Component {
  constructor(props, context) {
    super(props, context);
  }

  onAddStarted() {
    this.props.navigation.navigate('TaskForm')
  }

  onDone(todo) {
    this.props.actions.deleteTodo(todo);
  }

  render() {
    return (
        <TaskList todos={this.props.todos}
          onAddStarted={this.onAddStarted.bind(this)}
          onDone={this.onDone.bind(this)} />
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    todos: state.todos
  };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(todoActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PluralTodo);