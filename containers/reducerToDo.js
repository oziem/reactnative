import {ADD_TODO, DEL_TODO} from '../ActionTypes'

const initialTodos = {
    todos: [
        { task: 'Learn React Native' },
        { task: 'Learn Redux' },
      ]
}

export default function todoReducer(state = initialTodos.todos, action) {
    switch(action.type) {
        case ADD_TODO:
          console.log(action.todo.task)
          return [
            ...state,
            Object.assign({}, action.todo)
          ];
        case DEL_TODO:
          console.log(action.todo.task)
          return [
            ...state.filter(todo => todo.task != action.todo.task)
          ];
        default:
          return state;
    }
}