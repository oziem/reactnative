import {ADD_TODO, DEL_TODO} from '../ActionTypes'

export function addTodo(todo) {
    return {
        type: ADD_TODO,
        todo: todo
    }
}

export function deleteTodo(todo) {
    return {
        type: DEL_TODO,
        todo: todo
    }
}