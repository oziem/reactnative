import { combineReducers } from 'redux'
import todos from '../containers/reducerToDo'

const rootReducer = combineReducers({
    todos
});

export default rootReducer;