import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { PrimaryBackgroundColor } from './styles';
import PluralTodo from './containers/PluralTodo/PluralTodo';
import TaskForm from './containers/TaskForm/TaskForm';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux';

const store = configureStore();

const SimpleApp = StackNavigator({
  PluralTodo: {
    screen: PluralTodo,
    navigationOptions: {
      headerTitle: 'Home',
    },
  },
  TaskForm: {
    screen: TaskForm,
    navigationOptions: {
      headerTitle: 'TaskForm',
    },
  },
});

export default class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <SimpleApp/>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: PrimaryBackgroundColor,
    flex: 1,
    marginTop: StatusBar.currentHeight
  },
});
