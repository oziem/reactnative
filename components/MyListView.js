import React from 'react'
import PropTypes from 'prop-types'
import { ListView } from 'react-native'

const ds = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2
});

const MyListView = ({ data, renderRow }) => (
    <ListView
        key={data}
        enableEmptySections
        dataSource={ds.cloneWithRows(data)}
        renderRow={renderRow}
    />
)

MyListView.propTypes = {
    renderRow: PropTypes.func.isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default MyListView