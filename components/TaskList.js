import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, ListView } from 'react-native'
import MyListView from './MyListView'
import Button from './Button'
import { PrimaryBackgroundColor, AccentColor } from '../styles'
import TaskRow from './TaskRow'

const TaskList = ({ todos, onAddStarted, onDone }) => (
    <View style={styles.container} >
        <MyListView data={todos}
            renderRow={(todo) => 
                <TaskRow todo={todo} onDone={onDone} />} />
        
        <Button 
            text = 'Add One'
            onPress={onAddStarted}
            additionalStyle={styles.button}/>
    </View>
)

TaskList.propTypes = {
    todos: PropTypes.arrayOf(PropTypes.object).isRequired,
    onDone: PropTypes.func.isRequired,
    onAddStarted: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: PrimaryBackgroundColor,
        paddingTop: 0,
        flex: 1,
        justifyContent: 'flex-start'
    },
    button: {
        height: 60,
        borderColor: AccentColor,
        borderWidth: 2,
        backgroundColor: '#333',
        borderRadius: 5,
        marginTop: 0,
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#fafafa',
        fontSize: 20,
        fontWeight: '600',
    },
});

export default TaskList