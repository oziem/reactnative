import React from 'react'
import PropTypes from 'prop-types'
import { AccentColor } from '../styles'
import { Text, View, StyleSheet, TouchableNativeFeedback } from 'react-native'

const Button = ({ text, onPress, additionalStyle = {} }) => (
    <TouchableNativeFeedback 
        useForeground={true}
        background={TouchableNativeFeedback.SelectableBackgroundBorderless()}
        onPress={onPress}>
        <View  style={[styles.button, additionalStyle]}>
        <Text style={styles.buttonText}>
            {text}
        </Text>
        </View>
    </TouchableNativeFeedback>
)

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    additionalStyle: PropTypes.number,
}

const styles = StyleSheet.create({
    button: {
        height: 45,
        alignSelf: 'stretch',
        backgroundColor: AccentColor,
        borderRadius: 5,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#fafafa',
        fontSize: 18,
        fontWeight: '600',
    },
})

export default Button