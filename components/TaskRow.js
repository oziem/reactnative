import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, StyleSheet, View } from 'react-native'
import Button from './Button';

const TaskRow = ({ todo, onDone }) => (
    <View style={style.container}>
        <Text style={style.label}>{todo.task}</Text>
        <Button text='Done' 
            onPress={()=>onDone(todo)}
            additionalStyle={style.doneButton}/>
    </View>
)

TaskRow.propTypes = {
    onDone: PropTypes.func.isRequired,
    todo: PropTypes.shape({
        task: PropTypes.string.isRequired
    }).isRequired
}

const style = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#E7E7E7',
        borderRadius: 5,
        padding: 20,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 20,
        marginRight: 20
    },
    label: {
        maxWidth: 275,
        fontSize: 20,
        fontWeight: '300',
        marginRight: 5
    },
    doneButton: {
        height: 35,
        padding: 5,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
    },
})

export default TaskRow