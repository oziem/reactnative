import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, TextInput } from 'react-native'
import Button from './Button'
import { PrimaryBackgroundColor } from '../styles'

const AddTaskForm = ({ onAdd, onCancel, onChangeText }) => (
    <View style={styles.container}>
        <TextInput style={styles.input}
            onChangeText={onChangeText}/>
        
        <Button 
            text = 'Add'
            onPress={onAdd}/>
        <Button 
            text = 'Cancel'
            onPress={onCancel}
            additionalStyle={styles.cancelButton}/>
    </View>
)

AddTaskForm.propTypes = {
    onAdd: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onChangeText: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        paddingTop: 150,
        backgroundColor: PrimaryBackgroundColor,
    },
    input: {
        borderWidth: 1,
        borderColor: '#a7a7a7',
        height: 50,
        marginLeft: 10,
        marginRight: 10,
        padding: 15,
        borderRadius: 5,
    },
    cancelButton: {
        backgroundColor: '#666',
    },
});

export default AddTaskForm